const {BlockChain, Transaction} = require('./src/blockchain');
const EC = require('elliptic').ec;
const ec = new EC('secp256k1');

const testKey = ec.keyFromPrivate('bc8a5894d0b507d29677eeaeb06d58af9d45cfa08c9365d174c0864b812c3ca9');
const testWalletAddress = testKey.getPublic('hex')

let testCoin = new BlockChain();

const trans1 = new Transaction(testWalletAddress, 'some public key', 12)
trans1.signTransaction(testKey);
testCoin.addTransaction(trans1);

console.log('start miner')
testCoin.minePendingTransactions(testWalletAddress)

console.log(testCoin.getBalanceOfAddress(testWalletAddress))

console.log(JSON.stringify(testCoin.chain, null, 4))

console.log(testCoin.isChainValid())

testCoin.chain[1].transactions[0].amount = 23;

console.log(testCoin.isChainValid())
